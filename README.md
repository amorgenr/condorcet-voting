# Condorcet Voting

Condorcet Voting Simulation for Mathematics in Politics and Law Project

1st change

**References**\
    -[Impartial culture model](https://en.wikipedia.org/wiki/Impartial_culture)\
    -[Van Deemen, Adrian. "On the empirical relevance of Condorcet’s paradox." Public Choice 158.3-4 (2014): 311-330.](https://link.springer.com/article/10.1007/s11127-013-0133-3)\
    -[Gehrlein, William V. "Condorcet's paradox and the likelihood of its occurrence: different perspectives on balanced preferences." Theory and decision 52.2 (2002): 171-199.](https://link.springer.com/content/pdf/10.1023/A:1015551010381.pdf)