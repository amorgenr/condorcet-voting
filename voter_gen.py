import numpy as np;

def voting(nr_voters, nr_pols, min_unif, max_unif, mu_normal, sigma_normal, p_bernoulli, lp_norm):
    # voter generation, where nr_unif properties are uniformly distributed
    # and nr_normal properties are normally distributed.
    voters_unif = np.random.uniform(min_unif, max_unif, (nr_voters, np.size(max_unif)));
    voters_normal = np.random.normal(mu_normal, sigma_normal, (nr_voters, np.size(sigma_normal)));
    voters_bernoulli = np.random.binomial(1, p_bernoulli, (nr_voters, np.size(p_bernoulli)));
    voters = np.concatenate((voters_unif, voters_normal, voters_bernoulli), axis = 1);

    #politician generation, same as above
    pols_unif = np.random.uniform(min_unif, max_unif, (nr_pols, np.size(max_unif)));
    pols_normal = np.random.normal(mu_normal, sigma_normal, (nr_pols, np.size(sigma_normal)));
    pols_bernoulli = np.random.binomial(1, p_bernoulli, (nr_pols, np.size(p_bernoulli)));
    politicians = np.concatenate((pols_unif, pols_normal, pols_bernoulli), axis = 1);

    d = np.zeros((2, nr_pols))
    rankings = np.zeros((nr_voters, nr_pols))
    #find distance of voters to pols, and create their rankings
    for i in range(0, nr_voters):
        for j in range(0, nr_pols):
            d[0,j] = np.linalg.norm((voters[i] - politicians[j]), lp_norm)
            d[1,j] = j
        #sort d on row 0, so that row 1 is ranking for voter i
        dsorted = d[:,d[0,:].argsort()]
        rankings[i,:] = dsorted[1,:]
    return rankings

#returns if i comes before j in array
def i_before_j(a, i, j, i_max):
    for n in range(0, i_max):
        if a[n] == i:
            return True
        if a[n] == j:
            return False


def analyze_rankings(rankings, nr_pols, nr_voters):
    preferences = np.zeros((nr_pols, nr_pols))
    for i in range(0, nr_pols):
        for j in range(i, nr_pols):
            sum_i = 0
            sum_j = 0
            for n in range(0, nr_voters):
                if i_before_j(rankings[n], i, j, nr_pols):
                    sum_i += 1
                else:
                    sum_j += 1
            if sum_i > sum_j:
                preferences[i,j] = 1
            else:
                preferences[j,i] = 1
            if i == j:
                preferences[i,j] = 1
    total = np.sum(preferences, axis = 1)
    for i in total:
        if i == nr_pols:
            return False
    #find out if there is a candidate that for all other candidates more than 50 % prefer the first
    return True

def param_gen(nr_unif, nr_normal, nr_bernoulli):
    max_unif = np.random.rand(nr_unif)
    max_unif.fill(1)
    min_unif = -max_unif
    mu_normal = np.zeros(nr_normal)
    #mu_normal = (np.random.rand(nr_normal) - 0.5) / 10
    sigma_normal = np.reciprocal(np.linspace(1, nr_normal, num = nr_normal))/5
    sigma_normal.fill(0.2)
    p_bernoulli = np.zeros(nr_bernoulli)
    p_bernoulli.fill(0.5)

    print(max_unif, sigma_normal, p_bernoulli)
    return min_unif, max_unif, mu_normal, sigma_normal, p_bernoulli


def pol_test(min_pol, max_pol, nr_voters, nr_unif, nr_normal, nr_bernoulli, iterations, lp_norm):
    min_unif, max_unif, mu_normal, sigma_normal, p_bernoulli = param_gen(nr_unif, nr_normal, nr_bernoulli)
    results = np.zeros((max_pol - min_pol, 1))
    for nr_pols in range(min_pol, max_pol):
        for i in range(0, iterations):
            rankings = voting(nr_voters, nr_pols, min_unif, max_unif, mu_normal, sigma_normal, p_bernoulli, lp_norm)
            nocycle = analyze_rankings(rankings, nr_pols, nr_voters)
            print(nr_pols, nocycle)
            results[nr_pols - min_pol] += nocycle
    print(results/iterations)

def voter_test(min_vot, max_vot, nr_pols, nr_unif, nr_normal, nr_bernoulli, iterations, lp_norm):
    min_unif, max_unif, mu_normal, sigma_normal, p_bernoulli = param_gen(nr_unif, nr_normal, nr_bernoulli)
    #determine number of iterations
    its = int(np.ceil(np.log2((max_vot / min_vot))))
    results = np.zeros((its, 1))
    nr_voters = min_vot
    j = 0
    while nr_voters < max_vot:
        for i in range(0, iterations):
            rankings = voting(nr_voters, nr_pols, min_unif, max_unif, mu_normal, sigma_normal, p_bernoulli, lp_norm)
            nocycle = analyze_rankings(rankings, nr_pols, nr_voters)
            print(nr_voters, nocycle)
            results[j] += nocycle
        j += 1
        nr_voters *= 2
    print(results/iterations)

#pol_min, pol_max, voters, unif, norm, bern, it, lpnorm, scale
#pol_test(3, 20, 1000, 1, 10, 3, 100, 2)

#vot_min, vot_max, pols, unif, norm, bern, it, lpnorm
voter_test(10, 10000, 10, 4, 10, 3, 10, 2)
