import numpy as np;
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cmx
from mpl_toolkits.mplot3d import Axes3D

def voting(nr_voters, nr_pols, nr_unif, nr_normal, nr_bernoulli):
    # voter generation, where nr_unif properties are uniformly distributed
    # and nr_normal properties are normally distributed.
    voters_unif = np.random.uniform(-unif_magn, unif_magn, (nr_voters, nr_unif));
    voters_normal = np.random.normal(normal_mean, normal_std, (nr_voters, nr_normal));
    voters_bernoulli = np.random.binomial(1, bernoulli_p, (nr_voters, nr_bernoulli));
    voters = np.concatenate((voters_unif, voters_normal, voters_bernoulli), axis = 1);

    #politician generation, same as above
    pols_unif = np.random.uniform(-unif_magn, unif_magn, (nr_pols, nr_unif));
    pols_normal = np.random.normal(normal_mean, normal_std, (nr_pols, nr_normal));
    pols_bernoulli = np.random.binomial(1, bernoulli_p, (nr_pols, nr_bernoulli));
    politicians = np.concatenate((pols_unif, pols_normal, pols_bernoulli), axis = 1);

    d = np.zeros((2, nr_pols))
    rankings = np.zeros((nr_voters, nr_pols))
    #find distance of voters to pols, and create their rankings
    for i in range(0, nr_voters):
        for j in range(0, nr_pols):
            d[0,j] = np.linalg.norm((voters[i] - politicians[j]), lp_norm)
            d[1,j] = j
        #sort d on row 0, so that row 1 is ranking for voter i
        dsorted = d[:,d[0,:].argsort()]
        rankings[i,:] = dsorted[1,:]
    return rankings

#returns if i comes before j in array
def i_before_j(a, i, j, i_max):
    for n in range(0, i_max):
        if a[n] == i:
            return True
        if a[n] == j:
            return False


def analyze_rankings(rankings, nr_pols, nr_voters, nr_unif, nr_normal, nr_bernoulli):
    preferences = np.zeros((nr_pols, nr_pols))
    for i in range(0, nr_pols):
        for j in range(i, nr_pols):
            sum_i = 0
            sum_j = 0
            for n in range(0, nr_voters):
                if i_before_j(rankings[n], i, j, nr_pols):
                    sum_i += 1
                else:
                    sum_j += 1
            if sum_i > sum_j:
                preferences[i,j] = 1
            elif sum_j > sum_i:
                preferences[j,i] = 1
            if i == j:
                preferences[i,j] = 1
    total = np.sum(preferences, axis = 1)
    for i in total:
        if i == nr_pols:
            return False
    #find out if there is a candidate that for all other candidates more than 50 % prefer the first
    return True

#Parameters
unif_magn = 1
normal_mean = 0
normal_std = 0.2
bernoulli_p = 0.5
lp_norm = 2


def pol_test(min_pol, max_pol, nr_voters, nr_unif, nr_normal, nr_bernoulli, iterations):
    results = np.zeros((max_pol - min_pol, 1))
    for nr_pols in range(min_pol, max_pol):
        for i in range(0, iterations):
            rankings = voting(nr_voters, nr_pols, nr_unif, nr_normal, nr_bernoulli)
            nocycle = analyze_rankings(rankings, nr_pols, nr_voters, nr_unif, nr_normal, nr_bernoulli)
            print(nr_pols, nocycle)
            results[nr_pols - min_pol] += nocycle
        print(results[nr_pols - min_pol]/iterations*100)
    print(results/iterations)

def voter_test(min_vot, max_vot, nr_pols, nr_unif, nr_normal, nr_bernoulli, iterations):
    its = int(np.ceil(np.log2((max_vot/min_vot))))
    results = np.zeros((its, 1))
    nr_voters = min_vot
    j = 0
    while nr_voters < max_vot:
        #nr_voters += 1
        for i in range(0, iterations):
            rankings = voting(nr_voters, nr_pols, nr_unif, nr_normal, nr_bernoulli)
            nocycle = analyze_rankings(rankings, nr_pols, nr_voters, nr_unif, nr_normal, nr_bernoulli)
            print(nr_voters, nocycle)
            results[j] += nocycle
        print(results[j]/iterations*100)
        j += 1
        #nr_voters -= 1
        nr_voters *= 2
    print(results/iterations)

def par_num_test(nr_voters, nr_pols, min_num_params, max_num_params, iterations):
    results = []
    for nr_unif in range(0, max_num_params + 1):
        temp = max_num_params - nr_unif
        min_num_params = max(0, (min_num_params - nr_unif))
        for nr_normal in range(0, max_num_params + 1):
            temp2 = temp - nr_normal
            min_num_params = max(0, (min_num_params - nr_normal))
            for nr_bernoulli in range (min_num_params, temp2 + 1):
                counter = 0
                for i in range(0, iterations):
                    rankings = voting(nr_voters, nr_pols, nr_unif, nr_normal, nr_bernoulli)
                    nocycle = analyze_rankings(rankings, nr_pols, nr_voters, nr_unif, nr_normal, nr_bernoulli)
                    counter += nocycle
                results.append([nr_unif, nr_normal, nr_bernoulli, counter])
                print(nr_unif,nr_normal,nr_bernoulli,counter)
    return results, iterations



def scatter3d(x,y,z, cs, colorsMap='jet'):
    cm = plt.get_cmap(colorsMap)
    cNorm = matplotlib.colors.Normalize(vmin=0, vmax=iterations)
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.scatter(x, y, z, c=scalarMap.to_rgba(cs))
    ax.set_xlabel('Uniform')
    ax.set_ylabel('Normal')
    ax.set_zlabel('Bernoulli')
    scalarMap.set_array(cs)
    fig.colorbar(scalarMap)
    plt.show()


#pol_min, pol_max, voters, unif, norm, bern, it
pol_test(100, 101, 1000, 3, 10, 1, 20)

#vot_min, vot_max, pols, unif, norm, bern, it
#voter_test(262145, 262146, 10, 1, 10, 3, 100)

#results, iterations = par_num_test(500,10,1,10,100)
#R = np.array(results)
#scatter3d(R[:,0], R[:,1], R[:,2], R[:,3])


